import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './hello-world.component';
import { BarsComponent } from './bars.component';
import { BarsWithScaleComponent } from './bars-with-scale/bars-with-scale.component';
import { XScaleComponent } from './bars-with-scale/x-scale.component';
import { YScaleComponent } from './bars-with-scale/y-scale.component';
import { TimelineComponent } from './timeline/timeline.component';

const routes: Routes = [
  { path: 'hello-world', component: HelloWorldComponent },
  { path: 'bars', component: BarsComponent },
  { path: 'bars-with-scale', component: BarsWithScaleComponent },
  { path: 'timeline', component: TimelineComponent },
  { path: '', component: HelloWorldComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    BarsComponent,
    BarsWithScaleComponent,
    XScaleComponent,
    YScaleComponent,
    TimelineComponent
  ],
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
